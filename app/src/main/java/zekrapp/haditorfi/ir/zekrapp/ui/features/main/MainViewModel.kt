package zekrapp.haditorfi.ir.zekrapp.ui.features.main

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import zekrapp.haditorfi.ir.zekrapp.model.db.ZekrDatabase
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
import zekrapp.haditorfi.ir.zekrapp.model.repository.ZekrRepository

class MainViewModel(application: Application) : AndroidViewModel(application) {
    val getAll: LiveData<List<ZekrEntity>>
    private val repository: ZekrRepository

    init {
        val zekrDao = ZekrDatabase.getInstance(application).zekrDao()
        repository = ZekrRepository(zekrDao = zekrDao)
        getAll = repository.getAll
    }

    fun addManyZekr(item: List<ZekrEntity>) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addManyZekr(item)
        }
    }

    fun addZekr(item: ZekrEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addZekr(item)
        }
    }

    fun updateZekr(item: ZekrEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateZekr(item)
        }
    }

    fun deleteZekr(item: ZekrEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteZekr(item)
        }
    }
}

class MainViewModelFactory(
    private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(application) as T
        }
        throw IllegalArgumentException("کلاس ویو مدل یافت نشد.")
    }

}