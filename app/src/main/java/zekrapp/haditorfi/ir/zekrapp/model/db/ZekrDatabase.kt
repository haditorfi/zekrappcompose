package zekrapp.haditorfi.ir.zekrapp.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import zekrapp.haditorfi.ir.zekrapp.model.dao.ZekrDao
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity

@Database(entities = [ZekrEntity::class], version = 1, exportSchema = false)
abstract class ZekrDatabase : RoomDatabase() {
    abstract fun zekrDao(): ZekrDao

    companion object {
        @Volatile
        private var INSTANCE: ZekrDatabase? = null

        fun getInstance(context: Context): ZekrDatabase {
            synchronized(this) {
                return INSTANCE ?: Room.databaseBuilder(
                    context.applicationContext,
                    ZekrDatabase::class.java,
                    "zekr_database"
                ).build().also {
                    INSTANCE = it
                }
            }
        }
    }
}