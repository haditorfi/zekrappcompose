package zekrapp.haditorfi.ir.zekrapp.ui

import android.app.Application
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.ExperimentalMaterialApi
import androidx.lifecycle.ViewModelProvider
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModel
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModelFactory
import zekrapp.haditorfi.ir.zekrapp.ui.theme.ZekrappComposeTheme
import zekrapp.haditorfi.ir.zekrapp.util.Navigation

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // to initialize the ViewModel.
        val viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        setContent {
            ZekrappComposeTheme {
                Navigation(viewModel)
            }
        }
    }
}

