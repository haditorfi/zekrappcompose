package zekrapp.haditorfi.ir.zekrapp.ui.features

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModel
import zekrapp.haditorfi.ir.zekrapp.ui.theme.AquaBlue
import zekrapp.haditorfi.ir.zekrapp.ui.theme.ButtonBlue
import zekrapp.haditorfi.ir.zekrapp.ui.theme.DeepBlue
import zekrapp.haditorfi.ir.zekrapp.ui.theme.RedDark
import zekrapp.haditorfi.ir.zekrapp.util.toast

@Composable
fun AddZekrScreen(
    navController: NavController,
    viewModel: MainViewModel
) {
    val context = LocalContext.current
    val textTitle = remember { mutableStateOf("") }
    val textZekrAr = remember { mutableStateOf("") }
    val textZekrFa = remember { mutableStateOf("") }
    val textGoalNumber = remember { mutableStateOf("100") }
    val textZekrArError = remember { mutableStateOf(false) }
    val textGoalNumberError = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(AquaBlue)
    ) {
        Box(modifier = Modifier.background(DeepBlue)) {
            Toolbar() { navController.popBackStack() }
        }
        Text(
            text = "افزودن و ویرایش ذکر",
            style = MaterialTheme.typography.h1,
            textAlign = TextAlign.Center,
            color = ButtonBlue,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
        )
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .padding(16.dp)
        ) {
            OutlinedTextField(
                value = textTitle.value,
                onValueChange = { textTitle.value = it },
                label = { Text(text = "عنوان (اختیاری)") },
                placeholder = { Text(text = "عنوان (اختیاری)") },
                singleLine = true,
                textStyle = MaterialTheme.typography.h2,
                modifier = Modifier
                    .weight(0.7f)
            )
            OutlinedTextField(
                value = textGoalNumber.value,
                onValueChange = { textGoalNumber.value = it },
                label = { Text(text = "تعداد هدف") },
                placeholder = { Text(text = "تعداد هدف") },
                singleLine = true,
                isError = textGoalNumberError.value,
                textStyle = MaterialTheme.typography.body2,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier
                    .weight(0.3f)
                    .padding(start = 8.dp)
            )
        }
        OutlinedTextField(
            value = textZekrAr.value,
            onValueChange = { textZekrAr.value = it },
            label = { Text(text = "ذکر به زبان عربی") },
            placeholder = { Text(text = "ذکر به زبان عربی") },
            singleLine = true,
            isError = textZekrArError.value,
            textStyle = MaterialTheme.typography.h2,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )
        OutlinedTextField(
            value = textZekrFa.value,
            onValueChange = { textZekrFa.value = it },
            label = { Text(text = "ترجمه به فارسی (اختیاری)") },
            placeholder = { Text(text = "ترجمه به فارسی (اختیاری)") },
            singleLine = true,
            textStyle = MaterialTheme.typography.h2,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        )
        Row(
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(16.dp)
        ) {
            Button(
                onClick = {
                    if (textZekrAr.value == "") {
                        textZekrArError.value = true
                        toast(context, "لطفا ذکر به زبان عربی را وارد نمایید.")
                        return@Button
                    }
                    if (textGoalNumber.value == "") {
                        textZekrArError.value = false
                        textGoalNumberError.value = true
                        toast(context, "لطفا تعداد هدف را وارد نمایید.")
                        return@Button
                    } else {
                        textZekrArError.value = false
                        textGoalNumberError.value = false

                        viewModel.addZekr(
                            ZekrEntity(
                                0,
                                textTitle.value,
                                textZekrAr.value,
                                textZekrFa.value,
                                0,
                                textGoalNumber.value.toInt()
                            )
                        )
                        toast(context, "عملیات با موفقیت انجام شد.")
                        navController.popBackStack()
                    }
                },
                modifier = Modifier
                    .height(56.dp)
                    .padding(end = 8.dp)
                    .weight(0.7f),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(ButtonBlue)
            ) {
                Text(
                    text = "تایید",
                    style = MaterialTheme.typography.h1,
                    color = Color.White
                )
            }
            Button(
                onClick = {
                    navController.popBackStack()
                },
                modifier = Modifier
                    .height(56.dp)
                    .weight(0.3f),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(RedDark)
            ) {
                Text(
                    text = "انصراف",
                    style = MaterialTheme.typography.h1,
                    color = Color.White
                )
            }
        }
    }
}