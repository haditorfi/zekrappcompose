package zekrapp.haditorfi.ir.zekrapp.ui.features.main

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import kotlinx.coroutines.delay
import saman.zamani.persiandate.PersianDate
import zekrapp.haditorfi.ir.zekrapp.model.entity.initZekr
import zekrapp.haditorfi.ir.zekrapp.ui.features.ZekrItem
import zekrapp.haditorfi.ir.zekrapp.ui.theme.*
import zekrapp.haditorfi.ir.zekrapp.util.MyScreens
import zekrapp.haditorfi.ir.zekrapp.util.ZEKR_KEY
import zekrapp.haditorfi.ir.zekrapp.util.today

@ExperimentalMaterialApi
@Composable
fun MainScreen(
    navController: NavController,
    viewModel: MainViewModel
) {
    val zekrList by viewModel.getAll.observeAsState(listOf())
    LaunchedEffect(Unit) {
        delay(2000)
        if (zekrList.isEmpty()) viewModel.addManyZekr(initZekr)
    }
    if (zekrList.isEmpty()) {
        LoadingComponent()
    } else {
        Scaffold(
            floatingActionButton = {
                FloatingActionButton(
                    onClick = { navController.navigate(MyScreens.AddZekrScreen.route) },
                    backgroundColor = DeepBlue,
                    contentColor = Color.White,
                ) {
                    Icon(Icons.Filled.Add, "افزودن ذکر")
                }
            }
        ) {
            Column(
                modifier = Modifier
                    .padding(it)
                    .background(AquaBlue)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(DeepBlue)
                        .padding(15.dp),
                ) {
                    Text(
                        text = "ذکر های پر خیر و برکت",
                        style = MaterialTheme.typography.h2,
                        color = Color.White
                    )
                    Text(
                        text = today(),
                        style = MaterialTheme.typography.h2,
                        color = Color.White
                    )
                }
                Card(
                    elevation = 5.dp,
                    modifier = Modifier
                        .padding(horizontal = 16.dp, vertical = 8.dp)
                        .fillMaxWidth()
                        .clickable {
                                navController.currentBackStackEntry?.savedStateHandle?.set(ZEKR_KEY, zekrList[PersianDate().dayOfWeek()])
                                navController.navigate(MyScreens.InnerScreen.route)
                        }
                ) {
                    Column(
                        horizontalAlignment = CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.background(Color.White)
                    ) {
                        Row(
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .background(BlueDark)
                                .fillMaxWidth()
                                .padding(10.dp)
                        ) {
                            Text(
                                text = " ذکر روز ${PersianDate().dayName()}",
                                style = MaterialTheme.typography.h2,
                                color = Color.White
                            )
                            Text(
                                text = today(withDayName = false),
                                style = MaterialTheme.typography.h2,
                                color = Color.White
                            )
                        }
                        Text(
                            text = zekrList[PersianDate().dayOfWeek()].zekrAr,
                            style = MaterialTheme.typography.h1,
                            color = Purple700,
                            modifier = Modifier
                                .padding(8.dp)
                        )
                        Text(
                            text = zekrList[PersianDate().dayOfWeek()].zekrFa,
                            style = MaterialTheme.typography.h2,
                            color = Purple900,
                            modifier = Modifier
                                .padding(8.dp)
                        )
                    }
                }
                LazyColumn(
                    modifier = Modifier.padding(
                        start = 8.dp,
                        end = 8.dp,
                        top = 1.dp,
                        bottom = 48.dp
                    )
                ) {
                    items(zekrList.size) { index ->
                        ZekrItem(
                            zekr = zekrList[index],
                            navController = navController,
                            viewModel = viewModel
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun LoadingComponent() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = CenterHorizontally
    ) {
        CircularProgressIndicator(
            color = DeepBlue,
            modifier = Modifier
                .wrapContentWidth(CenterHorizontally)
        )
    }
}