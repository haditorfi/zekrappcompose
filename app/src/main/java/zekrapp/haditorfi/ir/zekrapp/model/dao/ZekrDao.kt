package zekrapp.haditorfi.ir.zekrapp.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Dao
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
@Dao
interface ZekrDao {
    @Query("SELECT * FROM tblZekr")
    fun getAll(): LiveData<List<ZekrEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMany(item: List<ZekrEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: ZekrEntity)

    @Update
    suspend fun update(item: ZekrEntity)

    @Delete
    suspend fun delete(item: ZekrEntity)

}