package zekrapp.haditorfi.ir.zekrapp.util

sealed class MyScreens(val route:String){
    object MainScreen : MyScreens("main_screen")
    object SplashScreen : MyScreens("splash_screen")
    object InnerScreen : MyScreens("inner_screen")
    object AddZekrScreen : MyScreens("add_zekr_screen")
}
