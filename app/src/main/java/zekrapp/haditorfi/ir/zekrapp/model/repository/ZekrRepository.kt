package zekrapp.haditorfi.ir.zekrapp.model.repository

import androidx.lifecycle.LiveData
import zekrapp.haditorfi.ir.zekrapp.model.dao.ZekrDao
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity

class ZekrRepository(private val zekrDao: ZekrDao) {
    val getAll: LiveData<List<ZekrEntity>> = zekrDao.getAll()
    suspend fun addZekr(item: ZekrEntity) = zekrDao.insert(item)
    suspend fun addManyZekr(item: List<ZekrEntity>) = zekrDao.insertMany(item)
    suspend fun updateZekr(item: ZekrEntity) = zekrDao.update(item)
    suspend fun deleteZekr(item: ZekrEntity) = zekrDao.delete(item)
}