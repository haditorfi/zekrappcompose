package zekrapp.haditorfi.ir.zekrapp.ui.features

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import zekrapp.haditorfi.ir.zekrapp.util.today

@Composable
fun Toolbar(onBackPressed: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            imageVector = Icons.Default.ArrowForward,
            contentDescription = "بازگشت",
            tint = Color.White, modifier = Modifier
                .size(24.dp)
                .clickable {
                    onBackPressed.invoke()
                }
        )
        Text(
            text = today(),
            style = MaterialTheme.typography.h2,
            color = Color.White,
        )
    }
}