package zekrapp.haditorfi.ir.zekrapp.util

import android.content.Context
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Path
import saman.zamani.persiandate.PersianDate
import saman.zamani.persiandate.PersianDateFormat
import kotlin.math.abs

fun today(
    withDayName: Boolean = true,
): String {
    var pattern = "Y/m/d"
    if (withDayName) pattern = "l Y/m/d"
    return PersianDateFormat(pattern).format(PersianDate())
}

fun Path.standardQuadFromTo(from: Offset, to: Offset) {
    quadraticBezierTo(
        from.x,
        from.y,
        abs(from.x + to.x) / 2f,
        abs(from.y + to.y) / 2f
    )
}

fun toast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

//vibrate
fun View.vibrate() = reallyPerformHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
private fun View.reallyPerformHapticFeedback(feedbackConstant: Int) {
    isHapticFeedbackEnabled = true
    performHapticFeedback(feedbackConstant, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING)
}