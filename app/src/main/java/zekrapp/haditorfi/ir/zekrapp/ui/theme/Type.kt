package zekrapp.haditorfi.ir.zekrapp.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDirection
import androidx.compose.ui.unit.sp
import zekrapp.haditorfi.ir.zekrapp.R

val gothicA1 = FontFamily(
    listOf(
        Font(R.font.primary_regular, FontWeight.Normal),
        Font(R.font.primary_bold, FontWeight.Medium),
        Font(R.font.primary_bold, FontWeight.Bold),
    )
)

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        color = DarkerButtonBlue,
        fontFamily = gothicA1,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    ),
    body2 = TextStyle(
        color = DarkerButtonBlue,
        fontFamily = gothicA1,
        fontWeight = FontWeight.Bold,
        fontSize = 18.sp,
        textDirection = TextDirection.Ltr
    ),
    h1 = TextStyle(
        color = DeepBlue,
        fontFamily = gothicA1,
        fontWeight = FontWeight.Bold,
        fontSize = 22.sp
    ),
    h2 = TextStyle(
        color = DeepBlue,
        fontFamily = gothicA1,
        fontWeight = FontWeight.Bold,
        fontSize = 18.sp
    )
)