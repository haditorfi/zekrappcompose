package zekrapp.haditorfi.ir.zekrapp.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "tblZekr")
@Parcelize
data class ZekrEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String = "",
    val zekrAr: String,
    val zekrFa: String,
    var currentNumber: Int = 0,
    val goalNumber: Int = 100
) : Parcelable

val initZekr = listOf(
    ZekrEntity(1, "ذکر روز شنبه","یا رَبَّ الْعالَمین", "ای پروردگار جهانیان"),
    ZekrEntity(2, "ذکر روز یک شنبه","یا ذَالْجَلالِ وَالْاِكْرام", "ای صاحب جلال و بزرگواری"),
    ZekrEntity(3, "ذکر روز دوشنبه","یا قاضِیَ الْحاجات", "ای برآورنده ی حاجت ها"),
    ZekrEntity(4, "ذکر روز سه شنبه","یا اَرْحَمَ الرّاحِمین", "ای مهربان ترین مهربانان"),
    ZekrEntity(5, "ذکر روز چهارشنبه","یا حَیُّ یا قَیّوم", "ای زنده ، ای پاینده"),
    ZekrEntity(
        6,
        "ذکر روز پنج شنبه","لا اِلهَ اِلّا اللهُ الْمَلِكُ الْحَقُّ الْمُبین",
        "نیست خدایی جز الله فرمانروای حق و آشكار"
    ),
    ZekrEntity(
        7,
        "ذکر روز جمعه","اَللّهُمَّ صَلِّ عَلی مُحَمَّدٍ وَ آلِ مُحَمَّد",
        "خدایا برمحمد و آل محمد درود فرست"
    ),
)
