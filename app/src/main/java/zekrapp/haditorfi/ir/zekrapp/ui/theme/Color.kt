package zekrapp.haditorfi.ir.zekrapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Purple900 = Color(0xFF3300A0)
val Teal200 = Color(0xFF03DAC5)

val DeepBlue = Color(0xff06164c)
val ButtonBlue = Color(0xff505cf3)
val DarkerButtonBlue = Color(0xff566894)
val AquaBlue = Color(0xFFF0F8F8)
val BlueViolet1 = Color(0xffaeb4fd)
val BlueViolet2 = Color(0xff9fa5fe)
val BlueViolet3 = Color(0xff8f98fd)
val BlueLight = Color(0xff00ddff)
val BlueDark = Color(0xff0099cc)
val GreenDark = Color(0xFF248027)
val RedDark = Color(0xFFD10B0B)