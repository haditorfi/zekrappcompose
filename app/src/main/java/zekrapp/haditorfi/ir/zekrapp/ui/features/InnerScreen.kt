package zekrapp.haditorfi.ir.zekrapp.ui.features

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModel
import zekrapp.haditorfi.ir.zekrapp.ui.theme.*
import zekrapp.haditorfi.ir.zekrapp.util.MyScreens
import zekrapp.haditorfi.ir.zekrapp.util.standardQuadFromTo
import zekrapp.haditorfi.ir.zekrapp.util.vibrate

@Composable
fun InnerScreen(
    navController: NavController,
    viewModel: MainViewModel,
    zekr: ZekrEntity,
) {
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                onClick = {  },
                backgroundColor = BlueDark,
                contentColor = Color.White,
            ) {
                Icon(Icons.Filled.Settings, "تنظیمات")
            }
        }
    ) {
        Box(
            modifier = Modifier
                .padding(it)
                .fillMaxSize()
                .background(DeepBlue)
        ) {
            Column {
                Toolbar() { navController.popBackStack() }
                ZekrSection(
                    zekr = zekr
                )
                CircularProgressbar(
                    modifier = Modifier.weight(1f),
                    viewModel = viewModel,
                    zekr = zekr,
                )
            }
        }
    }
}

@Composable
fun ZekrSection(
    modifier: Modifier = Modifier,
    zekr: ZekrEntity,
) {
    BoxWithConstraints(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .padding(16.dp)
            .clip(RoundedCornerShape(10.dp))
            .fillMaxWidth()
            .aspectRatio(1.9f)
            .background(BlueViolet3)
    ) {
        BoxWithConstraint()
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = zekr.zekrAr,
                style = MaterialTheme.typography.h1,
                color = Purple700,
                modifier = Modifier
                    .padding(start = 8.dp, end = 8.dp, top = 48.dp)
                    .weight(1f)
            )
            Text(
                text = zekr.zekrFa,
                style = MaterialTheme.typography.h2,
                color = Purple500,
                modifier = Modifier
                    .padding(start = 8.dp, end = 8.dp, bottom = 16.dp)
            )
        }
    }
}

@Composable
fun CircularProgressbar(
    modifier: Modifier = Modifier,
    viewModel: MainViewModel,
    zekr: ZekrEntity,
    colorCircle: Color = BlueDark,
    colorText: Color = DeepBlue,
    colorFront: Color = GreenDark,
    colorBack: Color = BlueLight,
) {
    val view = LocalView.current
    var number by remember {
        mutableStateOf(zekr.currentNumber)
    }
    val percentage = if (number > 0) number / zekr.goalNumber.toFloat() else 0f
    var animationPlayed by remember {
        mutableStateOf(false)
    }
    val curPercentage = animateFloatAsState(
        targetValue = if (animationPlayed) percentage else 0f,
        animationSpec = tween(
            durationMillis = 1000,
            delayMillis = 0
        )
    )
    LaunchedEffect(key1 = true) {
        animationPlayed = true
    }
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                if (number < zekr.goalNumber) {
                    zekr.currentNumber = ++number
                    viewModel.updateZekr(zekr)
                    view.vibrate()
                }
            }
    ) {
        Box(
            contentAlignment = Alignment.Center,
        ) {
            Canvas(modifier = Modifier.size(85.dp * 2f)) {
                drawCircle(
                    color = colorCircle,
                    radius = 200.0f,
                )
                drawArc(
                    color = colorBack,
                    startAngle = -90f,
                    sweepAngle = 360f,
                    useCenter = true,
                    style = Stroke(16.dp.toPx(), cap = StrokeCap.Round)
                )
                drawArc(
                    color = colorFront,
                    startAngle = -90f,
                    sweepAngle = 360 * curPercentage.value,
                    useCenter = false,
                    style = Stroke(16.dp.toPx(), cap = StrokeCap.Butt)
                )
            }
            Text(
                text = number.toString(),
                color = colorText,
                fontSize = 36.sp,
                fontWeight = FontWeight.Bold,
            )
        }
        Text(
            text = " تعداد هدف : ${zekr.goalNumber}",
            color = Color.White,
            textAlign = TextAlign.Center,
            fontSize = 20.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 30.dp)
        )
    }
}

@Composable
private fun BoxWithConstraintsScope.BoxWithConstraint() {
    val width = constraints.maxWidth
    val height = constraints.maxHeight

    // Medium colored path
    val mediumColoredPoint1 = Offset(0f, height * 0.3f)
    val mediumColoredPoint2 = Offset(width * 0.1f, height * 0.35f)
    val mediumColoredPoint3 = Offset(width * 0.4f, height * 0.05f)
    val mediumColoredPoint4 = Offset(width * 0.75f, height * 0.7f)
    val mediumColoredPoint5 = Offset(width * 1.4f, -height.toFloat())

    val mediumColoredPath = Path().apply {
        moveTo(mediumColoredPoint1.x, mediumColoredPoint1.y)
        standardQuadFromTo(mediumColoredPoint1, mediumColoredPoint2)
        standardQuadFromTo(mediumColoredPoint2, mediumColoredPoint3)
        standardQuadFromTo(mediumColoredPoint3, mediumColoredPoint4)
        standardQuadFromTo(mediumColoredPoint4, mediumColoredPoint5)
        lineTo(width.toFloat() + 100f, height.toFloat() + 100f)
        lineTo(-100f, height.toFloat() + 100f)
        close()
    }

    // Light colored path
    val lightPoint1 = Offset(0f, height * 0.35f)
    val lightPoint2 = Offset(width * 0.1f, height * 0.4f)
    val lightPoint3 = Offset(width * 0.3f, height * 0.35f)
    val lightPoint4 = Offset(width * 0.65f, height.toFloat())
    val lightPoint5 = Offset(width * 1.4f, -height.toFloat() / 3f)

    val lightColoredPath = Path().apply {
        moveTo(lightPoint1.x, lightPoint1.y)
        standardQuadFromTo(lightPoint1, lightPoint2)
        standardQuadFromTo(lightPoint2, lightPoint3)
        standardQuadFromTo(lightPoint3, lightPoint4)
        standardQuadFromTo(lightPoint4, lightPoint5)
        lineTo(width.toFloat() + 100f, height.toFloat() + 100f)
        lineTo(-100f, height.toFloat() + 100f)
        close()
    }
    Canvas(
        modifier = Modifier
            .fillMaxSize()
    ) {
        drawPath(
            path = mediumColoredPath,
            color = BlueViolet2
        )
        drawPath(
            path = lightColoredPath,
            color = BlueViolet1
        )
    }
}


