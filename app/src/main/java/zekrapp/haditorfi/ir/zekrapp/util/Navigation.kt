package zekrapp.haditorfi.ir.zekrapp.util

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
import zekrapp.haditorfi.ir.zekrapp.ui.features.AddZekrScreen
import zekrapp.haditorfi.ir.zekrapp.ui.features.InnerScreen
import zekrapp.haditorfi.ir.zekrapp.ui.features.SplashScreen
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainScreen
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModel


@ExperimentalMaterialApi
@Composable
fun Navigation(viewModel: MainViewModel) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = MyScreens.MainScreen.route
    ) {
        composable(MyScreens.SplashScreen.route) {
            SplashScreen(navController = navController)
        }
        composable(MyScreens.MainScreen.route) {
            MainScreen(navController = navController, viewModel = viewModel)
        }
        composable(MyScreens.InnerScreen.route) {
            val zekr =
                navController.previousBackStackEntry?.savedStateHandle?.get<ZekrEntity>(ZEKR_KEY)
            zekr?.let {
                InnerScreen(navController = navController, viewModel = viewModel, zekr = zekr)
            }
        }
        composable(MyScreens.AddZekrScreen.route) {
            AddZekrScreen(navController = navController, viewModel = viewModel)
        }
    }
}