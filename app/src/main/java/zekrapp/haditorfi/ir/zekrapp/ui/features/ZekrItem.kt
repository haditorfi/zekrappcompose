package zekrapp.haditorfi.ir.zekrapp.ui.features

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import zekrapp.haditorfi.ir.zekrapp.R
import zekrapp.haditorfi.ir.zekrapp.model.entity.ZekrEntity
import zekrapp.haditorfi.ir.zekrapp.ui.features.main.MainViewModel
import zekrapp.haditorfi.ir.zekrapp.ui.theme.Purple700
import zekrapp.haditorfi.ir.zekrapp.util.MyScreens
import zekrapp.haditorfi.ir.zekrapp.util.ZEKR_KEY

@ExperimentalMaterialApi
@Composable
fun ZekrItem(
    zekr: ZekrEntity,
    navController: NavController,
    modifier: Modifier = Modifier,
    viewModel: MainViewModel
) {
    Card(
        modifier = modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 5.dp,
        shape = RoundedCornerShape(10.dp),
        onClick = {
            navController.currentBackStackEntry?.savedStateHandle?.set(ZEKR_KEY, zekr)
            navController.navigate(MyScreens.InnerScreen.route)
        }
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zekr),
                contentDescription = "Icon",
                modifier = Modifier
                    .width(60.dp)
                    .height(90.dp)
                    .padding(8.dp)
            )
            Column(
                modifier = Modifier
                    .padding(start = 4.dp)
                    .weight(1f)
            ) {
                Text(
                    text = zekr.title,
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier
                       .padding(top = 0.dp, bottom = 8.dp)
                )
                Text(
                    text = zekr.zekrAr,
                    style = MaterialTheme.typography.h2,
                    color = Purple700,
                    modifier = Modifier
                        .padding(bottom = 28.dp)
                )
            }

            if (zekr.id > 7) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .height(70.dp)
                        .width(50.dp)
                        .padding(1.dp)
                        .clickable {
                             viewModel.deleteZekr(zekr)
                        }

                ) {
                    Icon(
                        imageVector = Icons.Default.MoreVert,
                        contentDescription = "More",
                    )
                }
            }
        }
    }
}
